const addBtn = document.querySelector("#add");
const subtractBtn = document.querySelector("#prev");
const resetBtn = document.querySelector("#reset");
const display = document.querySelector("#display");
let count = 0;
// Logic

addBtn.addEventListener("click", () => {
  count++;
  display.textContent = count;
});
resetBtn.addEventListener("click", () => {
  count = 0;
  display.textContent = 0;
});
subtractBtn.addEventListener("click", () => {
  count--;
  display.textContent = count;
});
